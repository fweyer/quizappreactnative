export class QuestionDto {
    constructor(id, question, image, optionOne, optionTwo, optionThree, optionFour, correctAnswer) {
        this.id = id;
        this.question = question;
        this.image = image;
        this.optionOne = optionOne;
        this.optionTwo = optionTwo;
        this.optionThree = optionThree;
        this.optionFour = optionFour;
        this.correctAnswer = correctAnswer
    }
}