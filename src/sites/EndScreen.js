import React from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, BackHandler } from 'react-native';
import { Button } from 'react-native-paper';
import AppContext from '../../AppContext';


export default function EndScreen(props) {
    //globalState
    const myContext = React.useContext(AppContext);

    //props
    const { score, numberOfQuestions } = props

    //style
    const { imageBackground, image, textErgebnis, textGlueckwunsch, textName, richtigeAntworten, btnStyle } = styles
    return (
        <ImageBackground style={imageBackground} source={require('../../assets/ic_bg.png')}>
            <Text style={textErgebnis}>Ergebnis</Text>
            <Image style={image} source={require('../../assets/ic_trophy.png')}></Image>
            <Text style={textGlueckwunsch}>Herzlichen Glückwunsch</Text>
            <Text style={textName}>{myContext.globalName}</Text>
            <Text style={richtigeAntworten}>Du hast {score} von {numberOfQuestions} richtig beantwortet</Text>
            <Button style={btnStyle} mode="contained" onPress={() => BackHandler.exitApp()}>Beenden</Button>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    imageBackground: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
    image: {
        marginBottom: 30,
        justifyContent: "center",
        alignSelf: "center"
    },
    textErgebnis: {
        alignSelf: 'center',
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 22
    },
    textGlueckwunsch: {
        alignSelf: 'center',
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 22
    },
    textName: {
        marginTop: 10,
        alignSelf: 'center',
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 22
    },
    richtigeAntworten: {
        marginTop: 10,
        alignSelf: 'center',
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 15
    },
    btnStyle: {
        margin: 10,
        padding: 5
    }
});
