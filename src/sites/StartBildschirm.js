import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ImageBackground, Modal, ToastAndroid } from 'react-native';
import { TextInput, Button } from 'react-native-paper';
import Quizfragen from './Quizfragen'
import AppContext from '../../AppContext';

export default function StartBildschirm() {
    // globalState
    const myContext = React.useContext(AppContext);

    //states
    const [text, setText,] = React.useState('');
    const [modalVisible, setModalVisible] = React.useState(false);

    function _goToNextActivity() {
        if (text === '') {
            ToastAndroid.show("Bitte gebe deinen Namen ein", ToastAndroid.SHORT);
        }
        else {
            myContext.setGlobalName(text)
            setModalVisible(true)
        }

    }

    return (
        <View style={styles.container}>
            <ImageBackground style={styles.image} source={require('../../assets/ic_bg.png')}>
                <View style={styles.quizAppBox}>
                    <Text style={styles.quizAppText}>Quizapp</Text>
                </View>
                <View style={styles.box}>
                    <Text style={styles.textWlkommen}>Wilkommen</Text>
                    <Text style={styles.textNachricht}>Bitte gebe deinen Namen ein.</Text>
                    <TextInput mode='outlined' style={styles.textinput} label="Name" value={text} onChangeText={text => setText(text)} />
                    <Button mode="contained" style={styles.button} onPress={() => _goToNextActivity()}>Start</Button>
                </View>
            </ImageBackground>
            <Modal visible={modalVisible} onRequestClose={() => setModalVisible(false)} >
                <Quizfragen />
            </Modal>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column"
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
    box: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        width: 'auto',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10
    },
    quizAppBox: {
        fontSize: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },
    quizAppText: {
        color: '#FFFFFF',
        fontSize: 15
    },
    textWlkommen: {
        fontSize: 30
    },
    textNachricht: {
        fontSize: 12,
        margin: 10
    },

    textinput: {
        //flexDirection: 'row', 
        alignSelf: 'stretch',
        margin: 10
    },
    button: {
        margin: 10,
        padding: 8,
        alignSelf: 'stretch',
    }
});

