import React from 'react';
import { StyleSheet, Text, View, Image, ToastAndroid } from 'react-native';
import { Button } from 'react-native-paper';
import OneTextview from '../components/OneTextview'
import dataList from '../Data'
import EndScreen from './EndScreen';

export default function Quizfragen() {

    //states
    const [currentQuestionSelected, setCurrentQuestionSelected] = React.useState(0);
    const [gameState, setGameState] = React.useState("Bestätigen");
    let [score, setScore] = React.useState(0);
    let [currentPosition, setCurrentPosition] = React.useState(0);

    //style
    const { container, oneTextview, imageStyle, textStyle, btnStyle } = styles

    function _btnClick() {
        if (currentQuestionSelected > 0 || gameState !== "Bestätigen")
            _handleGameState()
        else
            ToastAndroid.show("Bitte wählen Sie eine Frage aus", ToastAndroid.SHORT);
    }

    function _nextQuestion() {
        if (currentPosition < dataList.length - 1) setCurrentPosition(++currentPosition)
    };

    function _selectQuestion(number) {
        if (gameState === "Bestätigen") setCurrentQuestionSelected(number)
    }

    function _increaseScore() {
        if (currentQuestionSelected === dataList[currentPosition].correctAnswer) setScore(++score)
    }

    function _handleGameState() {
        switch (gameState) {
            case "Bestätigen":
                setGameState("Nächste Frage")
                break
            case "Nächste Frage":
                _increaseScore()
                setGameState("Bestätigen")
                _nextQuestion()
                setCurrentQuestionSelected(0)
                break
            case "Beenden":
                _increaseScore()
                setCurrentPosition(0)
                break
        }

        if (gameState === "Bestätigen" && currentPosition >= dataList.length - 1) {
            setGameState("Beenden")
        }

    }

    if (gameState === "Beenden" && currentPosition === 0) {
        return (<EndScreen score={score} numberOfQuestions={dataList.length} />)
    }

    return (
        <View style={container}>
            <Text style={textStyle}>{dataList[currentPosition].question}</Text>
            <Image
                style={imageStyle}
                source={dataList[currentPosition].image}
            />
            <OneTextview
                style={oneTextview} textValue={dataList[currentPosition].optionOne}
                selected={1 == currentQuestionSelected}
                showAnswer={"Nächste Frage" === gameState || "Beenden" === gameState}
                isCorrectAnswer={dataList[currentPosition].correctAnswer === 1}
                onClick={() => _selectQuestion(1)} />
            <OneTextview
                style={oneTextview} textValue={dataList[currentPosition].optionTwo}
                selected={2 == currentQuestionSelected}
                isCorrectAnswer={dataList[currentPosition].correctAnswer === 2}
                showAnswer={"Nächste Frage" === gameState || "Beenden" === gameState}
                onClick={() => _selectQuestion(2)} />
            <OneTextview
                style={oneTextview} textValue={dataList[currentPosition].optionThree}
                selected={3 == currentQuestionSelected}
                isCorrectAnswer={dataList[currentPosition].correctAnswer === 3}
                showAnswer={"Nächste Frage" === gameState || "Beenden" === gameState}
                onClick={() => _selectQuestion(3)} />
            <OneTextview
                style={oneTextview} textValue={dataList[currentPosition].optionFour}
                isCorrectAnswer={dataList[currentPosition].correctAnswer === 4}
                selected={4 == currentQuestionSelected}
                showAnswer={"Nächste Frage" === gameState || "Beenden" === gameState}
                onClick={() => _selectQuestion(4)} />

            <Button style={btnStyle} mode="contained" onPress={() => _btnClick()}>{gameState}</Button>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#faf7f7'
    },
    oneTextview: {
        alignSelf: 'stretch',
    },
    imageStyle: {
        margin: 10,
        alignSelf: 'center'
    },
    textStyle: {
        margin: 10,
        alignSelf: 'center',
        textAlign: 'center',
        color: '#363A43',
        fontSize: 22
    },
    btnStyle: {
        margin: 10,
        padding: 5
    }
});