import { QuestionDto } from "./model/QuastionsDto";

const dataList = []

dataList.push(new QuestionDto(
    1, "Zu welchem Land gehört diese Flagge?",
    require('../assets/ic_flag_of_australia.png'),
    "Angola", "Österreich", "Australien", "Armenien",
    3
));

dataList.push(new QuestionDto(
    2, "Zu welchem Land gehört diese Flagge?",
    require('../assets/ic_flag_of_argentina.png'),
    "Argentien", "Armenien", "Australien", "Honduras",
    1
));

dataList.push(new QuestionDto(
    3, "Zu welchem Land gehört diese Flagge?",
    require('../assets/ic_flag_of_brazil.png'),
    "Belarus", "Belgien",
    "Thailand", "Brasilien",
    4
));

dataList.push(new QuestionDto(
    4, "Zu welchem Land gehört diese Flagge?",
    require('../assets/ic_flag_of_belgium.png'),
    "Bahamas", "Belgien",
    "Dom. Republik", "Niederlande",
    2
));

dataList.push(new QuestionDto(
    5, "Zu welchem Land gehört diese Flagge?",
    require('../assets/ic_flag_of_fiji.png'),
    "Indien", "Frankreich",
    "Fiji", "Neuseeland",
    3
));

dataList.push(new QuestionDto(
    6, "Zu welchem Land gehört diese Flagge?",
    require('../assets/ic_flag_of_germany.png'),
    "Deutschland", "Georgien",
    "Griechenland", "Keines der Länder",
    1
));

dataList.push(new QuestionDto(
    7, "Zu welchem Land gehört diese Flagge?",
    require('../assets/ic_flag_of_denmark.png'),
    "Deutschland", "Ägypten",
    "Dänemark", "Türkei",
    3
));

dataList.push(new QuestionDto(
    8, "Zu welchem Land gehört diese Flagge?",
    require('../assets/ic_flag_of_india.png'),
    "Irland", "Iran",
    "Ungarn", "Indien",
    4
));

dataList.push(new QuestionDto(
    9, "Zu welchem Land gehört diese Flagge?",
    require('../assets/ic_flag_of_new_zealand.png'),
    "Australien", "Neuseeland",
    "Tuvalu", "USA",
    2

));

dataList.push(new QuestionDto(
    10, "Zu welchem Land gehört diese Flagge?",
    require('../assets/ic_flag_of_kuwait.png'),
    "Kuwait", "Jordanien",
    "Sudan", "Israel",
    1
));

export default dataList;
