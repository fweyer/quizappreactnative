import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default function SelectedTextview(props) {

    const { isSelectetContainer, isSelectedText } = styles;
    const { selected, onClick, textValue } = props;

    return (
        <TouchableOpacity style={isSelectetContainer} onPress={onClick}>
            <Text style={isSelectedText}>{textValue}</Text>
        </TouchableOpacity>
    )

}

const styles = StyleSheet.create({
    isSelectetContainer: {
        height: 50,
        alignSelf: 'stretch',
        borderColor: '#6200EE',
        borderWidth: 2,
        margin: 10,
        backgroundColor: '#FFFFFF',
    },
    isSelectedText: {
        flex: 1,
        fontSize: 20,
        alignSelf: 'center',
        paddingTop: 10,
        color: '#6200EE'
    }
});