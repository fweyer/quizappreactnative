import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import SelectedTextview from '../components/SelectedTexview'
import GreenTextview from '../components/GreenTextview'
import RedTextview from '../components/RedTextview'

export default function OneTextview(props) {

    //styles
    const { container, text, isSelectetContainer, isSelectedText } = styles;
    //props
    const { selected, onClick, textValue, isCorrectAnswer, showAnswer } = props;

    if (selected && !showAnswer) {
        return (
            <SelectedTextview selected={selected} textValue={textValue} onPress={onClick} />
        );
    }
    else if (selected && !isCorrectAnswer && showAnswer) {
        return (
            <RedTextview selected={selected} textValue={textValue} onPress={onClick} />
        );
    }
    else if (isCorrectAnswer && showAnswer) {
        return (
            <GreenTextview selected={selected} textValue={textValue} onPress={onClick} />
        );
    }
    return (
        <TouchableOpacity TouchableOpacity style={container} onPress={onClick} >
            <Text style={text}>{textValue}</Text>
        </TouchableOpacity >
    );
}

const styles = StyleSheet.create({
    container: {
        height: 50,
        alignSelf: 'stretch',
        borderColor: '#bab6b6',
        borderWidth: 1,
        margin: 10,
        backgroundColor: '#FFFFFF',
    },
    text: {
        flex: 1,
        fontSize: 20,
        alignSelf: 'center',
        paddingTop: 10,
        color: 'black'
    },
});
