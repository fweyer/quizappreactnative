import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default function RedTextview(props) {

    const { container, text } = styles;
    const { selected, onClick, textValue } = props;

    return (
        <TouchableOpacity style={container} onPress={onClick}>
            <Text style={text}>{textValue}</Text>
        </TouchableOpacity>
    )

}

const styles = StyleSheet.create({
    container: {
        height: 50,
        alignSelf: 'stretch',
        borderWidth: 1,
        margin: 10,
        color: '#FFFFFF',
        backgroundColor: 'red',
    },
    text: {
        flex: 1,
        fontSize: 20,
        alignSelf: 'center',
        paddingTop: 10,
        color: '#FFFFFF'
    }
});