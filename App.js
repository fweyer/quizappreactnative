import React, { useState } from 'react';
import { AppRegistry } from 'react-native';
import { Provider as PaperProvider } from 'react-native-paper';
import { name as appName } from './app.json';
import StartBildschirm from './src/sites/StartBildschirm';
import AppContext from './AppContext';

export default function App() {

  const [globalName, setGlobalName] = useState('globalname');

  const userSettings = {
    globalName: globalName,
    setGlobalName,
  }

  return (
    <AppContext.Provider value={userSettings}>
      <PaperProvider>
        <StartBildschirm />
      </PaperProvider>
    </AppContext.Provider>
  );
}

AppRegistry.registerComponent(appName, () => App);